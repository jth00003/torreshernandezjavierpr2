/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;


/**
 *
 * @author Javier
 */
public class Renderizador implements Callable<List<Escena>>{
    public static final int RETARDO = 1;
    static int ID=0;
    private final String idR;
    private static boolean terminadosGeneradores;
    List<Escena> resultado;
    Monitores monitor;
    
  /**
   * Constructor parametrizado por defecto de la clase monitor
   * @param monitor 
   */  
  public Renderizador(Monitores monitor){
    idR=""+ID++;
    terminadosGeneradores= false; 
    this.resultado=new LinkedList<>();
    this.monitor= monitor;
  }

    @Override
    public List<Escena> call() throws InterruptedException {
        Escena escena= null;
        boolean HayEscena= false;
        while (true){
            HayEscena= false;
            monitor.solicitaAccesoListas();
            if (monitor.isNoEmptyListaAltaPrioridad()){
                escena= monitor.SacarEscenaAltaPrioridad();
                HayEscena= true;
            }else{
                if (monitor.isNoEmptyListaBajaPrioridad()){
                    escena=monitor.SacarEscenaBajaPrioridad();
                    HayEscena= true;
                }else{
                    if(terminadosGeneradores){
                        monitor.liberaAccesoListas();
                        return resultado;
                    }
                }
            }
            monitor.liberaAccesoListas();
            if (HayEscena){
                System.out.println(escena.getIdE()+"Procesando Escena ");
                escena.setHInicProc(new Date());
                TimeUnit.SECONDS.sleep(escena.getDuracion());
                escena.setHFinProc(new Date());
                resultado.add(escena);
            }else{
                TimeUnit.SECONDS.sleep(RETARDO);
            }        
        }
    }
  
  public static void setTerminadosGeneradores(){
      terminadosGeneradores= true;
  }

}