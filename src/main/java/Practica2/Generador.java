/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;


import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static practica2.PRACTICA.generador;
import static practica2.Utilidades.VARIACION_NUM_ESCENAS;
import static practica2.Utilidades.VARIACION_TIEMPO;
import static practica2.Utilidades.MIN_NUM_ESCENAS;
import static practica2.Utilidades.MIN_TIEMPO_GENERACION;


/**
 *
 * @author Javier
 */
public class Generador implements Callable<List<Escena>>{
    static int ID=0;    
    String idG;
    Monitores monitor;
    CyclicBarrier barrera;
    List<Escena> resultado;
   
/**
 * Constructor parametrizado de la clase Generador
 * @param monitor to set
 * @param barrera to set
 */
public Generador( Monitores monitor, CyclicBarrier barrera ){
    idG=""+ID++;        
    this.resultado=new LinkedList<>();
    this.barrera= barrera;
    this.monitor= monitor;
}
/**
 * Funcion que genera las escenas  y las carga
 * @return 
 * @throws Exception 
 */
    @Override
   public List<Escena> call() throws Exception  {
    int nescenas= MIN_NUM_ESCENAS+generador.nextInt(VARIACION_NUM_ESCENAS);
    System.out.println("Se ha iniciado la ejecución de " + this.toString()+"");
    try{
        for (int i=0; i<nescenas; i++){
            int tEscena= MIN_TIEMPO_GENERACION+generador.nextInt(VARIACION_TIEMPO);  
            try {
                TimeUnit.SECONDS.sleep(tEscena);
            } catch (InterruptedException ex) {
                Logger.getLogger(Generador.class.getName()).log(Level.SEVERE, null, ex);
            }
            Escena escena= new Escena();
            System.out.println("Se ha creado la escena " + escena.getIdE());
            escena.inicializaEscena();
            resultado.add(escena);
            if (escena.isPrioridad()){
                  monitor.CargarEscenaAltaPrioridad(escena);
            }else{
                 monitor.CargarEscenaBajaPrioridad(escena);
            }        
        }
        this.barrera.await();
    }catch(InterruptedException e ){ 
        System.out.println("Se ha interrumpido la ejecución de " + this.toString());
    }finally {     
        System.out.println("Ha finalizado la ejecución de " + this.toString());
        return resultado;
   }   
  
}
    /**
     * Funcion que muestra los generadores
     */
    @Override
    public String toString(){
        return "ID del Generador"+ idG;
    }
    
}