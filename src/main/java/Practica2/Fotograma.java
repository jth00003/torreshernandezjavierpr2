/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;


import static practica2.PRACTICA.generador;
import static practica2.Utilidades.MIN_TIEMPO_FOTOGRAMA;
import static practica2.Utilidades.VARIACION_TIEMPO;

/**
 *
 * @author Javier
 */
public class Fotograma {
    static int ID=0;
    private final String idF;
    private int duracion;
    
    /**
     * Constructor por defecto de la clase Fotograma.
     */
    public Fotograma(  ){
        idF=""+ID++;
        duracion= MIN_TIEMPO_FOTOGRAMA+generador.nextInt(VARIACION_TIEMPO);        
    }
    
    /**
     * Funcion que muestra los fotogramas 
     */
    @Override
     public String toString(){
         return "ID del fotograma: " + idF + "\n" + "Duración del fotograma: " + duracion;
     }
       
    /**
     * @return Duracion
     */
    public int getDuracion() {
        return duracion;
    }
    
    /**
     * @param duracion the duracion to set
     */
    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
     
    /**
     * @return idF
     */
    public String getIdF() {
        return idF;
    }
    
}

