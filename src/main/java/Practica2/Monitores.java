/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 *
 * @author Javier
 */
public class Monitores {
    //variables Compartidas
        static Semaphore SemAltaPLleno;
        static Semaphore SemAltaPVacio;
        static List<Escena> EscenasAltaP;
        static Semaphore SemaBajaPLleno; 
        static Semaphore SemaBajaPVacio;
        static List<Escena> EscenasBajaP;
        static Semaphore mutexAltaP;
        static Semaphore mutexBajaP;
        static Semaphore mainMutex;
    
    /**
     * Constructor parametrizado por defecto de la clase monitor
     * @param max_escenas para el semaforo
     */    
    public Monitores(int max_escenas){
        SemAltaPLleno= new Semaphore(max_escenas); 
        SemAltaPVacio= new Semaphore(0);
        EscenasAltaP= new LinkedList<>();
        SemaBajaPLleno= new Semaphore(max_escenas); 
        SemaBajaPVacio= new Semaphore(0);
        EscenasBajaP= new LinkedList<>();
        mutexAltaP= new Semaphore(1);
        mutexBajaP= new Semaphore(1);
        Monitores.mainMutex = new Semaphore(1);
        
    }  
    /**
     * Funcion que solicita el acceso a la lista
     * @throws InterruptedException 
     */
    public void solicitaAccesoListas() throws InterruptedException{
        mainMutex.acquire();
    }
    
    /**
     * Funcion que libera el acceso a la lista
     * @throws InterruptedException 
     */
    public void liberaAccesoListas() throws InterruptedException{
        mainMutex.release();
    }
    /**
     * Funcion que carga las escenas de baja prioridad
     * @param escena a cargar
     * @throws InterruptedException 
     */
    public void CargarEscenaBajaPrioridad(Escena escena) throws InterruptedException{
        SemaBajaPLleno.acquire();
        mutexBajaP.acquire();
        try {
            EscenasBajaP.add(escena);
        } finally {    
            mutexBajaP.release(); 
            SemaBajaPVacio.release();
        }
    }
    
    /**
     * Funcion que carga las escenas de alta prioridad
     * @param escena a cargar
     * @throws InterruptedException 
     */
    public void CargarEscenaAltaPrioridad(Escena escena) throws InterruptedException{      
        SemAltaPLleno.acquire();
        mutexAltaP.acquire();
        try {
            EscenasAltaP.add(escena);
        } finally {
            mutexAltaP.release();            
            SemAltaPVacio.release();
        }
    }
    
    /**
     * Funcion que determina si esta vacia la lista de baja prioridad
     * @return bool
     */
    public boolean isNoEmptyListaBajaPrioridad(){
        return !EscenasBajaP.isEmpty();
    }
    
    /**
     * Funcion que determina si esta vacia la lista de alta prioridad
     * @return bool
     */
    public boolean isNoEmptyListaAltaPrioridad(){
        return !EscenasAltaP.isEmpty();
    }
    
    /**
     * Funcion que saca la escena de la lista de baja prioridad
     * @return escena retirada
     * @throws InterruptedException 
     */
    public Escena SacarEscenaBajaPrioridad() throws InterruptedException{
        Escena escena=null;
        if (Thread.interrupted())
            throw new InterruptedException();
        
        SemaBajaPVacio.acquire();
        mutexBajaP.acquire();
        try{ 
            escena= EscenasBajaP.get(0);
            EscenasBajaP.remove(0);
        } finally {
            mutexBajaP.release();
            SemaBajaPLleno.release();          
        }
        return escena;
    }
    
    /**
     * Funcion que saca la escena de la lista de alta prioridad
     * @return escena retirada
     * @throws InterruptedException 
     */
    public Escena SacarEscenaAltaPrioridad() throws InterruptedException{
        Escena escena=null;
        if (Thread.interrupted())
            throw new InterruptedException();
        
        SemAltaPVacio.acquire();
        mutexAltaP.acquire();
        try{ 
            escena= EscenasAltaP.get(0);
            EscenasAltaP.remove(0);
        } finally {
            mutexAltaP.release();
            SemAltaPLleno.release();
        }
        return escena;
    }
    
    
     
     
     
}

