/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import static practica2.PRACTICA.generador;
import static practica2.Utilidades.MIN_NUM_FOTOGRAMAS;
import static practica2.Utilidades.TIEMPO_FINALIZACION_ESCENA;
import static practica2.Utilidades.VARIACION_NUM_FOTOGRAMAS;


/**
 *
 * @author Javier
 */
public class Escena {    
    static int ID=0;
    private final String idE;
    private boolean prioridad;
    private int duracion;
    private List<Fotograma> listaFotogramas;
    private Date HGeneracion;
    private Date HInicProc;
    private Date HFinProc;   
    
    /**
     * Constructor por defecto de la clase Escena
     * @throws InterruptedException 
     */
    public Escena( ) throws InterruptedException{                 
        idE=""+ID++;       
        duracion= TIEMPO_FINALIZACION_ESCENA;
        int tipo= generador.nextInt(2);
        prioridad= ( tipo==0 ) ;
        this.listaFotogramas=new LinkedList<>();        
        HInicProc= new Date();
        HFinProc= new Date();        
   } 
    
   /**
    * Funcion que inicia las escenas.
    * Asigna el numero de fotogramas a generar y asigna la hora de inicio.
    */
   public void inicializaEscena(){
       setHGeneracion(new Date());  
       int numFotogramasAGenerar;
       numFotogramasAGenerar= MIN_NUM_FOTOGRAMAS+generador.nextInt(VARIACION_NUM_FOTOGRAMAS);        
       for (int i=0; i<numFotogramasAGenerar; i++){
           Fotograma fotog = new Fotograma();
            this.listaFotogramas.add(fotog);
            setDuracion(getDuracion() + fotog.getDuracion());
       }
       
   }

   
    /**
     * Funcion que muestra la escena 
     */
    @Override
    public String toString(){
        String cad= new String();
        cad= "Escena { \n" + "    Hora de generación: " + HGeneracion +"\n" +
                "    Hora de comienzo del renderizado: " + HInicProc + "\n" +
                "    Hora de finalización del renderizado: " + HFinProc  +"\n" +
                "    El proceso ha durado: " + TotalProcesamiento() + "ms" + " }";
        for (Fotograma f:listaFotogramas){
            cad=cad+ "\n" + "   --> " +f.toString() ;
        }
        cad=cad+"\n";
        
        return cad;
    }
    
    /**
     * 
     * @return La duración del procesamiento de la escena
     */
    public float TotalProcesamiento(){
        return (HFinProc.getTime()-HInicProc.getTime())/1000;
    }
    
    /**
     * @return idE
     */
    public String getIdE() {
        return idE;
    }

    /**
     * @return duracion
     */
    public int getDuracion() {
        return duracion;
    }

    /**
     * @param duracion the duracion to set
     */
    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    /**
     * @return prioridad
     */
    public boolean isPrioridad() {
        return prioridad;
    }

    /**
     * @param prioridad the prioridad to set
     */
    public void setPrioridad(boolean prioridad) {
        this.prioridad = prioridad;
    }

    /**
     * @return HGeneracion
     */
    public Date getHGeneracion() {
        return HGeneracion;
    }

    /**
     * @param HGeneracion the HGeneracion to set
     */
    public void setHGeneracion(Date HGeneracion) {
        this.HGeneracion = HGeneracion;
    }

    /**
     * @return HInicProc
     */
    public Date getHInicProc() {
        return HInicProc;
    }

    /**
     * @param HInicProc the HInicProc to set
     */
    public void setHInicProc(Date HInicProc) {
        this.HInicProc = HInicProc;
    }

    /**
     * @return HFinProc
     */
    public Date getHFinProc() {
        return HFinProc;
    }

    /**
     * @param HFinProc the HFinProc to set
     */
    public void setHFinProc(Date HFinProc) {
        this.HFinProc = HFinProc;
    }

    /**
     * @return listaFotogramas
     */
    public List<Fotograma> getListaFotogramas() {
        return listaFotogramas;
    }

    /**
     * @param listaFotogramas the listaFotogramas to set
     */
    public void setListaFotogramas(List<Fotograma> listaFotogramas) {
        this.listaFotogramas = listaFotogramas;
    }
    
    

}