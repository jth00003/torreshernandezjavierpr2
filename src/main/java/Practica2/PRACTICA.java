/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static practica2.Utilidades.NUM_RENDERIZADORES;
import static practica2.Utilidades.MAX_ESCENAS_EN_ESPERA;
import static practica2.Utilidades.NUM_GENERADORES;

/**
 *
 * @author Javier
 */
public class PRACTICA {

    public final static Random generador = new Random();
     
    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     * @throws java.util.concurrent.ExecutionException
     * @throws java.util.concurrent.BrokenBarrierException
     * @throws java.util.concurrent.TimeoutException
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException, BrokenBarrierException, TimeoutException {
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        ExecutorService ejecucion = (ExecutorService) Executors.newCachedThreadPool();              
             
        Monitores monitor = new Monitores(MAX_ESCENAS_EN_ESPERA);
        
        Finalizador finalizador = new Finalizador();
        CyclicBarrier barrera= new CyclicBarrier(NUM_GENERADORES, finalizador);
        
        ArrayList<Callable<List<Escena>>> tareas = new ArrayList<>();
        for (int i=0; i<NUM_GENERADORES; i++) {
            Generador genera = new Generador(monitor, barrera);
            tareas.add(genera);
        }
        for (int i=0; i<NUM_RENDERIZADORES; i++) {
            Renderizador renderizador = new Renderizador(monitor);
            tareas.add(renderizador);
        }

        List<Future<List<Escena>>> escenasFin = null;
        escenasFin = ejecucion.invokeAll(tareas);

        ejecucion.shutdown();
        ejecucion.awaitTermination(10, TimeUnit.SECONDS);

        float total=0f;
        int totalEscenas=0;
        for (int i=NUM_GENERADORES; i<NUM_GENERADORES+NUM_RENDERIZADORES; i++) {

            Future<List<Escena>> resultado = escenasFin.get(i);
             List<Escena> lista=resultado.get();
             totalEscenas+=lista.size();
             for(Escena datos:lista){
                 total+=datos.TotalProcesamiento();
                 System.out.println("La escena "+datos.getIdE()+" generÃ³ los siguientes datos :"+datos.toString());
             }
        }
        System.out.println("TotalEscenas procesadas: "+ totalEscenas);
        System.out.println("TotalProcesamiento: "+ total );

        System.out.println("Finalizaron todos los procesos");        

        System.out.println("Ha finalizado la ejecución del hilo principal");

    }
 
}
